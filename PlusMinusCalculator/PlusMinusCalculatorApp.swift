//
//  PlusMinusCalculatorApp.swift
//  PlusMinusCalculator
//  Created by brfsu on 11.03.2023.
//
import SwiftUI

@main
struct PlusMinusCalculatorApp: App
{
    var body: some Scene {
        WindowGroup {
            CalculatorView()
        }
    }
}
