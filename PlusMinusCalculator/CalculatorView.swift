//
//  ContentView.swift
//  PlusMinusCalculator
//  Created by brfsu on 11.03.2023.
//
import SwiftUI

struct CalculatorView: View
{
    @State private var displ = "0."
    @State private var wh = UIScreen.main.bounds.size.width / 5
    @State private var paddingValue: CGFloat = 3
//    @State private var mode = 0
    
    var body: some View {
        VStack {
//            TextField("", text: $displ ).keyboardType(.decimalPad)
//                    .frame(width: UIScreen.main.bounds.size.width - 70, height: 70)
//                    .border(.blue)
//                    .font(.system(size: 30, weight: .bold))
//                    .foregroundColor(Color.white)
//                    .background(Color.cyan)
//                    .clipShape(RoundedRectangle(cornerRadius: 15))
//                    .padding(3)
            
            VStack(alignment: .leading) {
                    Text(displ)
                    .frame(width: wh * 4 + 8 * paddingValue, height: wh - 20, alignment: .trailing)
                        .padding(10)
                        .font(.system(size: 50, weight: .regular))
                        .foregroundColor(Color.white)
                        .background(Color.cyan)
                        .clipShape(RoundedRectangle(cornerRadius: 15))
                        .padding(3)
                }
            
                HStack {
                    HStack {
                        ForEach(1 ..< 4) { index in
                            Button {
                                displ = CalcBrain.numberPressed(numPressed: String(index))
                            } label: {
                                ButtonCalc(buttonTitle: String(index), buttonLength: wh)
                            }
                        }
                    }
                    Button {
                        displ = CalcBrain.resetCalc()
                    } label: {
                        ButtonCalc(buttonTitle: "AC", buttonColor: .orange, buttonLength: wh)
                    }
                }
                
                HStack {
                    HStack {
                        ForEach(4 ..< 7) { index in
                            Button {
                                displ = CalcBrain.numberPressed(numPressed: String(index))
                            } label: {
                                ButtonCalc(buttonTitle: String(index), buttonLength: wh)
                            }
                        }
                    }
                    Button {
                        displ = CalcBrain.operation(oper: "+")
                    } label: {
                        ButtonCalc(buttonTitle: "+", buttonColor: .red, buttonLength: wh)
                    }
                }
                
                HStack {
                    HStack {
                        ForEach(7 ..< 10) { index in
                            Button {
                                displ = CalcBrain.numberPressed(numPressed: String(index))
                            } label: {
                                ButtonCalc(buttonTitle: String(index), buttonLength: wh)
                            }
                        }
                    }
                    Button {
                        displ = CalcBrain.operation(oper: "–")
                    } label: {
                        ButtonCalc(buttonTitle: "–", buttonColor: .cyan, buttonLength: wh)
                    }
                }
                
                HStack {
                    Button {
                        displ = CalcBrain.dotPressed()
                    } label: {
                        ButtonCalc(buttonTitle: ",", buttonColor: .yellow, buttonLength: wh)
                    }
                    
                    Button {
                        displ = CalcBrain.numberPressed(numPressed: "0")
                    } label: {
                        ButtonCalc(buttonTitle: "0", buttonLength: wh)
                    }
                    
                    Button {
                        displ = CalcBrain.changeSign()
                    } label: {
                        ButtonCalc(buttonTitle: "+/–", buttonColor: .yellow, buttonLength: wh)
                    }

                    Button {
                        displ = CalcBrain.doResult()
                    } label: {
                        ButtonCalc(buttonTitle: "=", buttonColor: .green, buttonLength: wh)
                    }
                    
                }
            }
        }//.onAppear(perform: buttonHandler)
//    func buttonHandler() {
//    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CalculatorView()
    }
}
