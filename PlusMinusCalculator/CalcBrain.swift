//
//  CalcBrain.swift
//  PlusMinusCalculator
//  Created by brfsu on 11.03.2023.
//
import Foundation

class CalcBrain
{
    static var signOper = ""
    static var displ = "0."
    static var inOperation = "."
    static var firstArg = 0.0
    static var secondArg = 0.0
    static var gettingArgum = 1
    static var numEditing = false
    static var drobn = false
    static var lastPressedButton = CalcButton.reset
    static var valueInDisplayIsPositive = true
    static var inResult = false

    static func resetCalc() -> String {
        signOper = ""
        displ = "0."
        inOperation = "."
        firstArg = 0.0
        secondArg = 0.0
        gettingArgum = 1
        numEditing = false
        drobn = false
        lastPressedButton = .reset
        valueInDisplayIsPositive = true
        inResult = false
        return "0."
    }

    
    static func operation(oper: String) -> String {
        if (gettingArgum == 2 &&
            lastPressedButton != .operation &&
            lastPressedButton != .result) {
            _ = doResult()
        }
        if signOper == "err" { signOper = ""}
        gettingArgum = 2
        firstArg = Double(displ)!
        inOperation = oper
        signOper = oper
        numEditing = false
        drobn = false
        inResult = false
        lastPressedButton = .operation
        return displ
    }

    
    static func numberPressed(numPressed: String) -> String {
        if signOper == "err" { signOper = ""}
        
        if lastPressedButton == .result {
            _ = resetCalc()
        }

        if numEditing {
            if displ.hasPrefix("-0.") && !drobn {
                displ = "-" + numPressed + "."
            } else if displ.hasPrefix("0.") && !drobn {
                displ = numPressed + "."
            } else if displ.hasSuffix(".") && !drobn {
                displ = displ.replacingOccurrences(of: ".", with: "")
                displ = displ + numPressed + "."
            } else {
                displ = displ + numPressed
            }
        } else {
            numEditing = true
            if !drobn {
                displ = numPressed + "."
            } else {
                displ = "0." + numPressed
            }
        }
        inResult = false
        lastPressedButton = .number
        return displ
    }
    
    
    static func dotPressed() -> String {
        if signOper == "err" { signOper = ""}
        
        if lastPressedButton != .result {
            if inOperation != "." {
                if !numEditing {
                    gettingArgum = 2
                    firstArg = Double(displ)!
                    numEditing = true
                    secondArg = 0
                    displ = "0."
                }
            }
        } else {
            _ = resetCalc()
        }
        drobn = true
        inResult = false
        lastPressedButton = .dot
        return displ
    }

    
    static func changeSign() -> String {
        if signOper == "err" { signOper = ""}

        if lastPressedButton == .reset {
            numEditing = true
        } else if lastPressedButton == .operation {
            numEditing = true
            displ = "0."
        }
        
        if displ.hasPrefix("-") {
            displ = displ.replacingOccurrences(of: "-", with: "")
        } else {
            displ = "-" + displ
        }
        valueInDisplayIsPositive = !valueInDisplayIsPositive
        lastPressedButton = .sign
        return displ
    }

    
    static func doResult() -> String {
        if signOper == "err" { signOper = ""}
        if inOperation != "." { // + - * /
            if gettingArgum == 2 {
                var result : Double
//                result = self.firstArg
//                self.secondArg = Double(displ)!
                
//                if (inResult &&
//                    lastPressedButton == .sign &&
//                    !valueInDisplayIsPositive) {
//                    result = Double(displ)!
//                } else {
//                    result = self.firstArg
//                    if lastPressedButton != .result {
//                        self.secondArg = Double(displ)!
//                    }
//                }
                
                if inResult {
                    result = Double(displ)!
                } else {
                    result = self.firstArg
                    self.secondArg = Double(displ)!
                }
                
                switch inOperation {
                case "+": result += self.secondArg
                case "–": result -= self.secondArg
                case "✕": result *= self.secondArg
                case "÷": result /= self.secondArg
                default: break
                }
//                self.signOper = ""
//                self.inOperation = "."
                self.firstArg = result
//                self.secondArg = 0
                self.numEditing = false
                self.drobn = false
                
                let resStr = String(result)
                if !resStr.hasSuffix("inf") && !resStr.hasSuffix("nan") {
                    if resStr.hasSuffix(".0") {
                        displ = resStr.replacingOccurrences(of: ".0", with: ".")
                    } else {
                        displ = resStr
                    }
                } else {
                    _ = resetCalc()
                    signOper = "err"
                    return displ
                }
            }
        }
        inResult = true
        lastPressedButton = .result
        return displ
    }
    
    
}


enum CalcButton {
    case number
    case operation
    case reset
    case result
    case dot
    case sign
}
