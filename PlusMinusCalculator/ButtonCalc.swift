//
//  ButtonUIView.swift
//  PlusMinusCalculator
//  Created by brfsu on 11.03.2023.
//
import SwiftUI

struct ButtonCalc: View
{
    var buttonTitle : String
    var buttonColor : Color = .blue
    var buttonSize : Int = 1
    var buttonLength : Double = 70

    var body: some View {
        let paddingValue: CGFloat = 3
        Text("\(buttonTitle)")
            .frame(width: buttonSize == 1 ? buttonLength : buttonLength * 2 + 4 * paddingValue, height: buttonSize == 1 ? buttonLength : buttonLength * 2 + 4 * paddingValue)
            .font(.system(size: 30, weight: .bold))
            .foregroundColor(Color.white)
            .background(buttonColor)
//            .clipShape(Circle())
            .clipShape(RoundedRectangle(cornerRadius: 15))
            .padding(paddingValue)
    }
}

struct ButtonUIView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonCalc(buttonTitle: "+", buttonSize: 1)
    }
}
